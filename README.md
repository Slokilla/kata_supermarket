Supermarket Pricing
---

The problem domain is something seemingly simple: pricing goods at supermarket.


Choices
---

Because the time was limited, so I made choices:

- The store have infinite stock. 
- There is no functional difference between "$/unit" and "$/pound". In the second
case, the unit price will be set to the pound price, and the user should enter mass
  rather than quantity
-There's no checkout phase, the total cart price
  is updated when his content is updated.
  
My implementation is oriented around a strategy-like pattern. We have a sort of repository 
named "PricingManager". Each product have his pricing strategy registered, and this strategy
is called when the product is added to the cart.

The core method is "**Pricing.apply(Cart)**". The Cart is here to explore each product. It will permit
us to implements some original pricing like :

```Buy 2 Shirts, get 1 Chocolate Cake free```

The PricingManager is a singleton, so when he is updated, he is updated for every class which uses it.

In this model, if we want to implement a save, we should save only PricingManager and a Stock.

I tried as I could to keep the float precision until the end to have the more precise results. 
The round takes place after the final pricing is calculated. Actually, i didn't had time to ensure
that it's always the case. 

