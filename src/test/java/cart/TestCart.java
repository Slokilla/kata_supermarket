package cart;

import pricing.NforMdollarsPricing;
import pricing.PricingManager;
import exceptions.CannotApplyOnZeroException;
import exceptions.CannotRemoveMoreThanQuantity;
import exceptions.NoUnitOfProductInCartException;
import org.junit.jupiter.api.Test;
import product.Product;

import static org.junit.jupiter.api.Assertions.*;

public class TestCart {
    @Test
    public void ShouldReturnTheGoodResultWhenProductHaveDiscount() {
        Product A = new Product("A", 2.70f);
        Product B = new Product("B", 4.70f);

        PricingManager pm = PricingManager.getPricingManager();
        try {
            pm.setPricing(A, new NforMdollarsPricing(A, 2, 3));
        } catch (CannotApplyOnZeroException e) {
            e.printStackTrace();
        }

        pm.setPricing(B);

        Cart cart = new Cart();

        cart.addProduct(A, 2);
        cart.addProduct(B, 2);

        assertEquals(Math.round((3f + 2 * 4.70f) * 100f) / 100f, cart.getFinalPrice());
    }

    @Test
    public void ShouldReturnTheGoodResultWhenProductHaveDiscountEvenIfWeRemoveSomeProducts() throws CannotRemoveMoreThanQuantity, NoUnitOfProductInCartException {
        Product A = new Product("A", 2.70f);
        Product B = new Product("B", 4.70f);

        PricingManager pm = PricingManager.getPricingManager();
        try {
            pm.setPricing(A, new NforMdollarsPricing(A, 2, 3));
        } catch (CannotApplyOnZeroException e) {
            e.printStackTrace();
        }

        pm.setPricing(B);

        Cart cart = new Cart();

        cart.addProduct(A, 2);
        cart.addProduct(B, 2);

        assertEquals(Math.round((3f + 2 * 4.70f) * 100f) / 100f, cart.getFinalPrice());

        cart.removeProduct(A, 1);

        assertEquals(Math.round((2.70f + 2 * 4.70f) * 100f) / 100f, cart.getFinalPrice());
    }

    @Test
    public void ShouldThrowAnErrorIfTryingToRemoveMoreItemsThanCartHave() {
        Product A = new Product("A", 2.70f);
        Product B = new Product("B", 4.70f);

        PricingManager pm = PricingManager.getPricingManager();
        try {
            pm.setPricing(A, new NforMdollarsPricing(A, 2, 3));
        } catch (CannotApplyOnZeroException e) {
            e.printStackTrace();
        }

        pm.setPricing(B);

        Cart cart = new Cart();

        cart.addProduct(A, 2);

        try {
            cart.removeProduct(A, 3);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        } catch (CannotRemoveMoreThanQuantity ignored) {
            return;
        }

        fail("It should have been fail because we remove more than we have");
    }

    @Test
    public void ShouldReturnTheGoodQuantityOfAGivenProduct() {
        Product A = new Product("A", 2.70f);

        PricingManager pm = PricingManager.getPricingManager();
        try {
            pm.setPricing(A, new NforMdollarsPricing(A, 2, 3));
        } catch (CannotApplyOnZeroException e) {
            e.printStackTrace();
        }

        Cart cart = new Cart();

        cart.addProduct(A, 5);
        cart.addProduct(A, 5);
        cart.addProduct(A, 5);

        try {
            assertEquals(15, cart.getProduct_quantity(A));
        } catch (NoUnitOfProductInCartException e) {
            fail("It should not have fail");
        }
    }
}
