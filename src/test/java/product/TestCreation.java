package product;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestCreation {
    @Test
    public void ShouldEndWhenCreatingProductWithNameAndPrice(){
        Product A = new Product("A", 2.70f);
        assertEquals(A.getPrice(), 2.70f);
        assertEquals(A.getName(), "A");
    }

    @Test
    public void ShouldFailWhenCreatingProductWithNameAndPriceAndTestOtherPrice(){
        Product A = new Product("A", 2.70f);
        assertNotEquals(A.getPrice(), 100f);
    }

}
