package pricing;

import cart.Cart;
import exceptions.CannotApplyOnZeroException;
import exceptions.NoUnitOfProductInCartException;
import org.junit.jupiter.api.Test;
import product.Product;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestNforMdollarsPricing {
    @Test
    public void ShouldReturnALinearPriceIfNisNotReach() throws CannotApplyOnZeroException {
        Product A = new Product("a", 2.78f);

        NforMdollarsPricing pricing = new NforMdollarsPricing(A,5,7.20f);

        Cart cart = new Cart();

        cart.addProduct(A,3);
        float result = 0;
        try {
            result = Math.round(pricing.apply(cart) * 100);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }

        assertEquals(834f, result);
    }

    @Test
    public void ShouldReturnAModulatePriceIfNisReach() throws CannotApplyOnZeroException {
        Product A = new Product("a", 2.78f);

        NforMdollarsPricing pricing = new NforMdollarsPricing(A,5,7.20f);

        Cart cart = new Cart();

        cart.addProduct(A,5);
        float result = 0;
        try {
            result = Math.round(pricing.apply(cart) * 100);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }

        assertEquals(720f, result);
    }

    @Test
    public void ShouldFailIfNIsZero(){
        Product A = new Product("a", 2.78f);

        NforMdollarsPricing pricing = null;
        try {
            pricing = new NforMdollarsPricing(A,0,7.20f);
        } catch (CannotApplyOnZeroException ignored) {
            return;
        }

        fail("It should had fail becaus N is zero");
    }
}
