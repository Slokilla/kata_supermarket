package pricing;

import cart.Cart;
import exceptions.NoUnitOfProductInCartException;
import org.junit.jupiter.api.Test;
import product.Product;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLinearPricing {
    @Test
    public void ShouldReturnTheGoodApply() {
        Product B = new Product("B", 4.78f);

        LinearPricing pricing = new LinearPricing(B);

        Cart cart = new Cart();

        cart.addProduct(B,10);
        float result = 0;
        try {
            result = Math.round(pricing.apply(cart)*100f);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }

        assertEquals(4780, result);
    }
}
