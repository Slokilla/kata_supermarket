package pricing;

import exceptions.CannotApplyOnZeroException;
import exceptions.NoDiscountOnProductException;
import org.junit.jupiter.api.Test;
import product.Product;

import static org.junit.jupiter.api.Assertions.*;

public class TestPricingManager {
    PricingManager pm = PricingManager.getPricingManager();

    @Test
    public void ShouldAddTheEntryWithLinearPricingByDefault() throws NoDiscountOnProductException {
        Product A = new Product("a", 2.78f);

        pm.setPricing(A);

        assertSame(pm.getPricingForProduct(A).getClass(), LinearPricing.class);
    }

    @Test
    public void ShouldAddTheEntryWithTheCreatedPricing() throws NoDiscountOnProductException, CannotApplyOnZeroException {
        Product A = new Product("a", 2.78f);

        pm.setPricing(A,new NforMdollarsPricing(A,2,3));

        assertSame(pm.getPricingForProduct(A).getClass(), NforMdollarsPricing.class);
    }

    @Test
    public void ShouldThrowAnExceptionWhenTryingToGetDeletedEntry() throws CannotApplyOnZeroException {
        Product A = new Product("a", 2.78f);

        pm.setPricing(A,new NforMdollarsPricing(A,2,3));

        try {
            assertSame(pm.getPricingForProduct(A).getClass(), NforMdollarsPricing.class);
        } catch (NoDiscountOnProductException ignored) {

        }

        try {
            pm.removePricing(A);
        } catch (NoDiscountOnProductException ignored) {
        }

        try {
            pm.removePricing(A);
        } catch (NoDiscountOnProductException ignored) {
            return;
        }

        fail("Remove should had throw an error, because A is no longer in the registry");


    }
}
