package pricing;

import cart.Cart;
import exceptions.CannotApplyOnZeroException;
import exceptions.NoUnitOfProductInCartException;
import org.junit.jupiter.api.Test;
import product.Product;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestBuyNgetMfree {

    @Test
    public void ShouldReturnTheLinearPriceWhenThresholdIsntReach() {
        Product A = new Product("a", 2.78f);
        BuyNgetMfree pricing = null;

        try {
            pricing = new BuyNgetMfree(A, 5, 1);
        } catch (CannotApplyOnZeroException e) {
            fail("It should'nt have failed");
        }

        Cart cart = new Cart();
        cart.addProduct(A, 1);

        float result = 0;

        try {
            result = Math.round(pricing.apply(cart) * 100);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }

        assertEquals(278f, result);
    }

    @Test
    public void ShouldReturnThePriceOfOnlyTwoProductBecauseThirdIsFree() {
        Product A = new Product("a", 2.78f);
        BuyNgetMfree pricing = null;

        try {
            pricing = new BuyNgetMfree(A, 2, 1);
        } catch (CannotApplyOnZeroException e) {
            fail("It should'nt have failed");
        }

        Cart cart = new Cart();
        cart.addProduct(A, 3);

        float result = 0;

        try {
            result = Math.round(pricing.apply(cart) * 100);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }

        assertEquals(556f, result);
    }

    @Test
    public void ShouldFailIfTryingWithZeroBoughtMGift() {
        Product A = new Product("a", 2.78f);
        BuyNgetMfree pricing = null;

        try {
            pricing = new BuyNgetMfree(A, 0, 1);
        } catch (CannotApplyOnZeroException e) {
            return;
        }

        fail("It should have failed");
    }


}
