package product;

public class Product {


    private String name;
    private float price;

    public Product(String _name, float _price) {
        this.name = _name;
        this.price = _price;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String _name) {
        this.name = _name;
    }

    public float getPrice() {
        return this.price;
    }

    public void setPrice(float _price) {
        this.price = _price;
    }
}
