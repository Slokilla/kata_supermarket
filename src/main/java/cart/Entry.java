package cart;

import exceptions.CannotRemoveMoreThanQuantity;

import java.util.ArrayList;

public class Entry {
    int number;
    float price;
    ArrayList<String> discountDescriptions = new ArrayList<>();

    public Entry(int number, float price, ArrayList<String> discountDescriptions) {
        this.setNumber(number);
        this.setPrice(price);
        this.setDiscountDescriptions(discountDescriptions);
    }

    public Entry(int number) {
        this.setNumber(number);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public ArrayList<String> getDiscountDescriptions() {
        return discountDescriptions;
    }

    public void setDiscountDescriptions(ArrayList<String> discountDescriptions) {
        this.discountDescriptions = discountDescriptions;
    }

    public void addSome(int n){
        setNumber(getNumber() + n);
    }

    public void removeSome(int n) throws CannotRemoveMoreThanQuantity {
        if (n > number) {
            throw new CannotRemoveMoreThanQuantity();
        }
        setNumber(getNumber() - n);
    }

    public void addDiscountDescription(String discountDescription) {
        discountDescriptions.add(discountDescription);
    }
}
