package cart;

import pricing.Pricing;
import pricing.PricingManager;
import exceptions.CannotRemoveMoreThanQuantity;
import exceptions.NoDiscountOnProductException;
import exceptions.NoUnitOfProductInCartException;
import product.Product;

import java.util.HashMap;
import java.util.Map;

public class Cart {
    /**
     * The map linking bought products and quantity.
     */
    HashMap<String, Entry> product_entry = new HashMap<>();

    PricingManager pm = PricingManager.getPricingManager();

    public Cart() {
    }

    public void addProduct(final Product _product, final int _quantity) {
        if (product_entry.containsKey(_product.toString())) {
            product_entry.get(_product.toString()).addSome(_quantity);
        } else {
            product_entry.put(
                    _product.toString(),
                    new Entry(_quantity)
            );
        }

        try {
            this.refreshPrice(_product);
        } catch (NoUnitOfProductInCartException e) {
            e.printStackTrace();
        }
    }

    public void removeProduct(final Product _product, final int _quantity) throws NoUnitOfProductInCartException, CannotRemoveMoreThanQuantity {
        if (product_entry.containsKey(_product.toString())) {
            if (product_entry.get(_product.toString()).number == 1) {
                product_entry.remove(_product.toString());
            } else {
                product_entry.get(_product.toString()).removeSome(_quantity);
            }

            this.refreshPrice(_product);
        } else {
            throw new NoUnitOfProductInCartException();
        }
    }

    public int getProduct_quantity(Product product) throws NoUnitOfProductInCartException {
        if (!product_entry.containsKey(product.toString())) {
            throw new NoUnitOfProductInCartException();
        }
        return product_entry.get(product.toString()).getNumber();
    }

    private void refreshPrice(Product _product) throws NoUnitOfProductInCartException {
        try {
            Pricing pricing = pm.getPricingForProduct(_product);

            product_entry.get(_product.toString()).setPrice(pricing.apply(this));
            product_entry.get(_product.toString()).addDiscountDescription(pricing.toString());
        } catch (NoDiscountOnProductException ignored){}
    }

    public float getFinalPrice() {
        float finalPrice = 0;
        for (Map.Entry<String, Entry> line : product_entry.entrySet()) {
            finalPrice += line.getValue().price;
        }
        return Math.round(finalPrice * 100f) / 100f;
    }
}
