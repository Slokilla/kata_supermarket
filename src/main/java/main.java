import cart.Cart;
import exceptions.CannotApplyOnZeroException;
import pricing.BuyNgetMfree;
import pricing.NforMdollarsPricing;
import pricing.PricingManager;
import product.Product;

public class main {
    public static void main(String[] args) {
        Product A = new Product("A", 2.70f);
        Product B = new Product("B", 4.70f);
        Product C = new Product("C", 4.00f);

        PricingManager pm = PricingManager.getPricingManager();
        try {
            pm.setPricing(A, new NforMdollarsPricing(A, 2, 3));
            pm.setPricing(B);
            pm.setPricing(C, new BuyNgetMfree(C, 2, 2));
        } catch (CannotApplyOnZeroException e) {
            e.printStackTrace();
        }

        Cart cart = new Cart();

        cart.addProduct(A, 2);
        cart.addProduct(B, 2);
        cart.addProduct(C, 4);

        //20.4
        System.out.println(cart.getFinalPrice());
    }
}

