package pricing;

import cart.Cart;
import exceptions.CannotApplyOnZeroException;
import exceptions.NoUnitOfProductInCartException;
import product.Product;

/**
 * This Class should be used when you want to apply "N units for M dollars"
 */
public class NforMdollarsPricing implements Pricing {

    private Product product;
    private int n;
    private float m;

    public NforMdollarsPricing(Product product, int n, float m) throws CannotApplyOnZeroException {
        this.setProduct(product);
        this.setN(n);
        this.setM(m);
    }

    @Override
    public float apply(Cart cart) throws NoUnitOfProductInCartException {
        float finalPrice = 0;
        int product_quantity = cart.getProduct_quantity(product);

        finalPrice = Math.floorDiv(product_quantity, n) * m;
        finalPrice += product_quantity % n * product.getPrice();

        return finalPrice;
    }

    @Override
    public String toString() {
        return String.format("Get %d for %f dollars", n, m);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) throws CannotApplyOnZeroException {
        if (n == 0) {
            throw new CannotApplyOnZeroException();
        }
        this.n = n;
    }

    public float getM() {
        return m;
    }

    public void setM(float m) {
        this.m = m;
    }
}
