package pricing;

import cart.Cart;
import exceptions.NoUnitOfProductInCartException;

public interface Pricing {
    float apply(final Cart cart) throws NoUnitOfProductInCartException;
}
