package pricing;


import exceptions.NoDiscountOnProductException;
import product.Product;

import java.util.HashMap;

public class PricingManager {
    private static PricingManager instance = null;

    /**
     * The map linking products and their discounts
     */
    HashMap<Product, Pricing> product_discounts = new HashMap<Product, Pricing>();

    private PricingManager() {
    }

    public static PricingManager getPricingManager() {
        if (instance == null) {
            instance = new PricingManager();
            return instance;
        }
        return instance;
    }

    public void setPricing(Product product, Pricing pricing) {
        if (product_discounts.containsKey(product)) {
            product_discounts.replace(product, pricing);
        } else {
            product_discounts.put(
                    product,
                    pricing
            );
        }
    }

    public void setPricing(Product product) {
        if (product_discounts.containsKey(product)) {
            product_discounts.replace(product, new LinearPricing(product));
        } else {
            product_discounts.put(
                    product,
                    new LinearPricing(product)
            );
        }
    }

    public void removePricing(Product product) throws NoDiscountOnProductException {
        if (product_discounts.containsKey(product)) {
            product_discounts.remove(product);
        } else {
            throw new NoDiscountOnProductException();
        }
    }

    public Pricing getPricingForProduct(Product product) throws NoDiscountOnProductException {
        if (product_discounts.containsKey(product)) {
            return product_discounts.get(product);
        }
        throw new NoDiscountOnProductException();
    }
}
