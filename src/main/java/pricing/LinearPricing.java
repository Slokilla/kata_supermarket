package pricing;

import cart.Cart;
import exceptions.NoUnitOfProductInCartException;
import product.Product;

/**
 * This is the default pricing method, price * quantity
 */
public class LinearPricing implements Pricing {

    private final Product product;

    public LinearPricing(Product product) {
        this.product = product;
    }

    @Override
    public float apply(Cart cart) throws NoUnitOfProductInCartException {
        return cart.getProduct_quantity(product) * product.getPrice();
    }

    @Override
    public String toString() {
        return "";
    }
}
