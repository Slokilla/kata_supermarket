package pricing;

import cart.Cart;
import exceptions.CannotApplyOnZeroException;
import exceptions.NoUnitOfProductInCartException;
import product.Product;

/**
 * This Class should be used when you want to apply "Buy N of a product, and the M next will cost 0"
 */
public class BuyNgetMfree implements Pricing {

    private Product product;
    private int n;
    private int m;

    public BuyNgetMfree(Product product, int n, int m) throws CannotApplyOnZeroException {
        this.setProduct(product);
        this.setN(n);
        this.setM(m);
    }

    /**
     * The method is called in order to calculate the price of a set of products in the cart
     * @param cart the cart on which the discount should be applied
     * @return float The total result for the product
     * @throws NoUnitOfProductInCartException if the cart have no product registered
     */
    @Override
    public float apply(Cart cart) throws NoUnitOfProductInCartException {
        float finalPrice = 0;
        int product_quantity = cart.getProduct_quantity(product);

        int freeToApply = 0;

        for (int i = 0; i < product_quantity; i++) {
            if (freeToApply == 0) {
                finalPrice += product.getPrice();
                if (i % n == 0) {
                    freeToApply = m;
                }
            } else {
                freeToApply--;
            }

        }

        return finalPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) throws CannotApplyOnZeroException {
        if (n == 0) {
            throw new CannotApplyOnZeroException();
        }
        this.n = n;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }
}
