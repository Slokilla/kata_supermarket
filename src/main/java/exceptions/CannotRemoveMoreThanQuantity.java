package exceptions;

public class CannotRemoveMoreThanQuantity extends Exception {
    public CannotRemoveMoreThanQuantity() {
    }

    public CannotRemoveMoreThanQuantity(String message) {
        super(message);
    }

    public CannotRemoveMoreThanQuantity(String message, Throwable cause) {
        super(message, cause);
    }
}
