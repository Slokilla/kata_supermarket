package exceptions;

public class NoDiscountOnProductException extends Exception{
    public NoDiscountOnProductException() {
        super();
    }

    public NoDiscountOnProductException(String message) {
        super(message);
    }

    public NoDiscountOnProductException(String message, Throwable cause) {
        super(message, cause);
    }
}
