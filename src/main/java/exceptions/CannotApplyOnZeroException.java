package exceptions;

public class CannotApplyOnZeroException extends Exception {
    public CannotApplyOnZeroException() {
        super();
    }

    public CannotApplyOnZeroException(String message) {
        super(message);
    }

    public CannotApplyOnZeroException(String message, Throwable cause) {
        super(message, cause);
    }
}
