package exceptions;

public class NoUnitOfProductInCartException extends Exception {
    public NoUnitOfProductInCartException() {
        super();
    }

    public NoUnitOfProductInCartException(String message) {
        super(message);
    }

    public NoUnitOfProductInCartException(String message, Throwable cause) {
        super(message, cause);
    }
}
